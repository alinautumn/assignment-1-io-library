section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
 .loop:
    cmp byte[rdi+rax], 0
    je .break_loop
    inc rax
    jmp .loop
 .break_loop:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, rsp
    dec rsp
    mov byte[rsp], 0
    mov r10, 10
  .loop:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    jne .loop
    mov rdi, rsp
    push rcx
    call print_string
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdx
  .loop:
    mov al, byte[rdi]
    mov cl, byte[rsi]
    cmp al, cl
    jne .not_equals
    cmp al, 0
    je .equals
    inc rdi
    inc rsi
    jmp .loop
  .not_equals:
    mov rax, 0
    pop rdx
    ret
  .equals:
    mov rax, 1
    pop rdx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor r8, r8
    dec rsi
  
  .loop:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, ' '
    je .reading
    cmp rax, `\t`
    je .reading
    cmp rax, '\n'
    je .reading
    mov r8, 1
    jmp .write_symbol

  .reading:
    test r8, r8
    jz .loop
    jmp .read_word_end

  .write_symbol:
    cmp rdx, rsi
    ja .check_overflow
    test al, al
    jz .read_word_end
    mov byte[rdi+rdx], al
    inc rdx
    jmp .loop
  
  .read_word_end:
    mov rax, rdi
    mov byte [rdi + rdx], 0
    ret

  .check_overflow:
    xor rax, rax
    xor rdx, rdx
    ret
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx

  .loop:
    mov dl, [rdi + rcx]
    cmp dl, '0'
    jl .break_loop
    cmp dl, '9'
    jg .break_loop
    sub dl, '0'
    imul rax, 10
    add rax, rdx
    inc rcx
    jmp .loop
    
  .break_loop:
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .int_neg
    jmp parse_uint

  .int_neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
  .loop:
    cmp rax, rdx
    je .error
    mov r9b, [rdi+rax]
    mov [rsi+rax], r9b
    test r9b, r9b
    je .return
    inc rax
    jmp .loop
  .error:
    xor rax, rax
  .return:
    ret
